const removerTelaLogin = () => {
  let sectionLogin = document.getElementById('login')
  sectionLogin.classList.add('FaltaImplementar')

  let lgpdLogin = document.getElementById('lgpd-login')
  lgpdLogin.classList.add("FaltaImplementar")
}

const removerTelaCadastro = () => {
  let sectionCadastro = document.getElementById('cadastro')
  sectionCadastro.classList.add('FaltaImplementar')
  let linkWpp = document.getElementById('link-wpp')
  linkWpp.classList.add("FaltaImplementar")
}

const removerTelaCadastroSegundoPasso = () => {
  let sectionCadastroSegundoPasso = document.getElementById(
    'cadastro-segundo-passo'
  )
  sectionCadastroSegundoPasso.classList.add('FaltaImplementar')

  let boxPrincipal = document.getElementById('menu-principal')

  boxPrincipal.classList.remove('cadastro-2')

  let linkWpp = document.getElementById('link-wpp')
  linkWpp.classList.add("FaltaImplementar")
}


const removerTelaRecuperarSenha = () =>{
  let sectionRecuperSenha = document.getElementById("recuperar-senha")
  sectionRecuperSenha.classList.add('FaltaImplementar')
}

const removerTelaRecuperarSenhaSegundoPasso=()=>{
  let sectionRecuperSenhaSegundoPasso = document.getElementById("recuperar-senha-final")
  sectionRecuperSenhaSegundoPasso.classList.add('FaltaImplementar')
}

const removerCadastroTerceiroPasso = ()=>{
  let sectionCadastroTerceiroPasso = document.getElementById("cadastro-terceiro-passo")
  sectionCadastroTerceiroPasso.classList.add('FaltaImplementar')
  let linkWpp = document.getElementById('link-wpp')
  linkWpp.classList.add("FaltaImplementar")
}





const adicionarTelaLogin = () => {
  let sectionLogin = document.getElementById('login')
  sectionLogin.classList.remove('FaltaImplementar')
  let lgpdLogin = document.getElementById('lgpd-login')
  lgpdLogin.classList.remove("FaltaImplementar")
}

const adicionarTelaCadastro = () => {
  let sectionCadastro = document.getElementById('cadastro')
  sectionCadastro.classList.remove('FaltaImplementar')
  let linkWpp = document.getElementById('link-wpp')
  linkWpp.classList.remove("FaltaImplementar")
}

const adicionarTelaCadastroSegundoPasso = () => {
  let sectionCadastroSegundoPasso = document.getElementById(
    'cadastro-segundo-passo'
  )
  sectionCadastroSegundoPasso.classList.remove('FaltaImplementar')

  let boxPrincipal = document.getElementById('menu-principal')

  boxPrincipal.classList.add('cadastro-2')
  let linkWpp = document.getElementById('link-wpp')
  linkWpp.classList.remove("FaltaImplementar")
}


const adicionarTelaRecuperarSenha = () =>{
  let sectionRecuperSenha = document.getElementById("recuperar-senha")
  sectionRecuperSenha.classList.remove('FaltaImplementar')
}

const adicionarTelaRecuperarSenhaSegundoPasso = () =>{
  let sectionRecuperSenhaSegundoPasso = document.getElementById("recuperar-senha-final")
  sectionRecuperSenhaSegundoPasso.classList.remove('FaltaImplementar')
}


const adicionarTelaCadastroTerceiroPasso = ()=>{
  let sectionCadastroTerceiroPasso = document.getElementById("cadastro-terceiro-passo")
  sectionCadastroTerceiroPasso.classList.remove('FaltaImplementar')
  let linkWpp = document.getElementById('link-wpp')
  linkWpp.classList.remove("FaltaImplementar")
}











  let botaoVoltarTelaLogin = document.getElementById('voltar-tela-login')

  botaoVoltarTelaLogin.addEventListener('click', ()=>{
    
    removerTelaRecuperarSenhaSegundoPasso()
    adicionarTelaLogin()
  })


  let botaoVoltarTelaLoginCadastro = document.getElementById('voltar-tela-login-cadastro')

  botaoVoltarTelaLoginCadastro.addEventListener('click', ()=>{
    removerCadastroTerceiroPasso()
    
    adicionarTelaLogin()
  })









// ================= Mostrar e Esconder Senha =======================


  
    const inputSenha = document.getElementById('senha-login')
    const iconOculto = document.getElementById("hide")
    const iconExibir = document.getElementById("show")


    iconOculto.addEventListener('click', ()=>{
      iconOculto.classList.add('FaltaImplementar')
      iconExibir.classList.remove('FaltaImplementar')
    
      inputSenha.type = "text"
     
    })

  
    iconExibir.addEventListener('click', ()=>{

    iconOculto.classList.remove("FaltaImplementar")
    iconExibir.classList.add("FaltaImplementar")

    inputSenha.type="password"

})
  
// ========================== AVALIÇÕES INPUTS ================================


// =============================LOGIN================================

const getEmailLogin = ()=>{
  let emailInput = document.getElementById('email-login')
  // return emailInput.value

 return emailInput.value
}

const getSenhaLogin = ()=>{
  let senhaInput = document.getElementById('senha-login')
  return senhaInput.value
}



const verificarEmail = (email)=>{
  if(getEmailLogin() == email){
    return alert("passou")
  }
  let msgErro = document.getElementById("email-login-erro")
  return msgErro.classList.remove("FaltaImplementar")
}

const verificarSenha = (senha)=>{
  if(getSenhaLogin()==senha){
    return alert("passou")
  }
  let msgErro = document.getElementById("senha-login-erro")
  return msgErro.classList.remove("FaltaImplementar")

}




const botaoLogar = document.getElementById("botao-login")

botaoLogar.addEventListener('click', ()=>{
 
  verificarEmail("rozuanny")
  verificarSenha("123")
})


// ============================== CADASTR0 ================================

const getEmail = () => {
  let inputEmail = document.getElementById('email-cadastro')
  return  inputEmail.value
}

const setEmail = () => {
  let email = getEmail()

  let element = document.getElementById('pegar-email-usuario')

  element.textContent = email
}

const statusCheckBox = id => {
  let checkBox = document.getElementById(id)
  return checkBox.checked
}

const validarEmailCadastro = (email)=>{

  if(getEmail() !== ""){


  if(getEmail()!==email){
    return true
  }
 }

 let msgErro = document.getElementById('check-erro-mensagem')
 msgErro.textContent = "Email Inválido"

 let msgErroAbrir = document.getElementById('mensagem-erro')
 msgErroAbrir.classList.remove('FaltaImplementar')

}

const BotaoCriarContaMedKey = document.getElementById('criar-minha-conta-medkey')

BotaoCriarContaMedKey.addEventListener('click', () => {
  
  if(!statusCheckBox('check-lgpd')){
    let msgErro = document.getElementById('check-erro-mensagem')
    msgErro.textContent = "È necessario aceitar os termos"

    let msgErroAbrir = document.getElementById('mensagem-erro')
    msgErroAbrir.classList.remove('FaltaImplementar')
     return
  }
  if (validarEmailCadastro("rozuanny")) {
    removerTelaCadastro()
    adicionarTelaCadastroSegundoPasso()
    setEmail()
    return
  }
  return 
})



// ============================== CADASTR0 2 PASSO  ================================
const verificarNome = () =>{
  let inputNomeCompleto = document.getElementById("nome-completo")
  if(inputNomeCompleto.value == "" || inputNomeCompleto == "  "){

    let msgErro = document.getElementById('nome-completo-cadastro')
    msgErro.classList.remove("FaltaImplementar")


    return false
  }
  return true
}

const verificarCpf = (cpf)=>{
  let inputCpf = document.getElementById('cpf')

  if(inputCpf.value.length != 11){
    let p = document.getElementById('texto-cpf-erro')
    p.textContent = "CPF está digitado errado"

    let msgErro = document.getElementById('cpf-cadastro')
    msgErro.classList.remove('FaltaImplementar')

    return
  }
  if(inputCpf.value == cpf ){
    let msgErro = document.getElementById('cpf-cadastro')
    msgErro.classList.remove('FaltaImplementar')
    return
  }
  if(inputCpf.value == ""){
    let p = document.getElementById('texto-cpf-erro')
    p.textContent = "CPF é obrigatorio"

    let msgErro = document.getElementById('cpf-cadastro')
    msgErro.classList.remove('FaltaImplementar')

    return 
  }

  return true
}

const verificarTelefone = ()=>{
  let inputTelefone = document.getElementById("telefone")
  if(inputTelefone.value == ""||inputTelefone.value == " "){

    let msgErro = document.getElementById("telefone-cadastro")
    msgErro.classList.remove("FaltaImplementar")
    return false
  }
  return true
}


const verificarSenhaAcesso = ()=>{
  const senhaAcesso = document.getElementById("senha-de-acesso").value
  if(senhaAcesso != "" ){
    return true
  }
  let msgErro = document.getElementById('senha-acesso-cadastro')
    msgErro.classList.remove('FaltaImplementar')
  return
}

const limpaErro = ()=>{
  let nome = document.getElementById('nome-completo-cadastro').classList.add("FaltaImplementar")
        
  let cpf= document.getElementById('cpf-cadastro').classList.add("FaltaImplementar")
  let tel =document.getElementById('telefone-cadastro').classList.add("FaltaImplementar")
  let senha =document.getElementById('senha-acesso-cadastro').classList.add("FaltaImplementar")
}


let botaoFinalizarCadastro = document.getElementById('finalizar-cadastro')

botaoFinalizarCadastro.addEventListener('click', ()=>{
  if(verificarCpf("08492068558") && verificarSenhaAcesso() && verificarNome() && verificarTelefone()){

    removerTelaCadastroSegundoPasso()
    adicionarTelaCadastroTerceiroPasso()
  }
})



// ================================= RECUPERAR SENHA ================================

const BotaoEnviarEmailRecuperacaoSenha = document.getElementById("enviar-email-recuper-senha")

const validarRecuperação = (entrada)=>{
 if(entrada.length==11 && !isNaN(entrada)){
   // if(entrada == cpf-cliente) {}
  return true
 }else {
    //if(entrada == email-cliente){}
 }

 return false
}

let inputRecuperacao = document.getElementById('email-recuperar-senha')


BotaoEnviarEmailRecuperacaoSenha.addEventListener('click',()=>{
  
  if(validarRecuperação(inputRecuperacao.value)){
    removerTelaRecuperarSenha()
    adicionarTelaRecuperarSenhaSegundoPasso()
      return
  }
  

    let msgErro = document.getElementById('recuperar-senha-erro')
    msgErro.classList.remove("FaltaImplementar")

})